package xyz.sky03.dao;

import xyz.sky03.domain.User;

import java.util.List;

public interface UserDao {
    /**
     * 查询所有的方法
     * @return
     */
    List<User> findAll();
}
